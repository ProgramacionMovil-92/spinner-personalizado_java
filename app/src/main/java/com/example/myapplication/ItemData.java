package com.example.myapplication;

public class ItemData {
    // Declarar variables
    private String textCategoria;
    private String textDescripcion;
    private Integer imageId;

    // Constructor
    public ItemData(String textCategoria, String textDescripcion, Integer imageId) {
        this.textCategoria = textCategoria;
        this.textDescripcion = textDescripcion;
        this.imageId = imageId;
    }

    // Getters & Setters

    public String getTextCategoria() {
        return textCategoria;
    }

    public void setTextCategoria(String textCategoria) {
        this.textCategoria = textCategoria;
    }

    public String getTextDescripcion() {
        return textDescripcion;
    }

    public void setTextDescripcion(String textDescripcion) {
        this.textDescripcion = textDescripcion;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

}
