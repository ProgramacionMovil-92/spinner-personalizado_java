package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    // Declarar Variables
    Spinner sp;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData(getString(R.string.itemFrappses),getString(R.string.msgFrappses),R.drawable.categorias));
        list.add(new ItemData(getString(R.string.itemAgradecimiento),getString(R.string.msgAgradecimiento),R.drawable.agradecimiento));
        list.add(new ItemData(getString(R.string.itemAmor),getString(R.string.msgAmor),R.drawable.corazon));
        list.add(new ItemData(getString(R.string.itemNewYear),getString(R.string.msgNewYear), R.drawable.nuevo));
        list.add(new ItemData(getString(R.string.itemCanciones),getString(R.string.msgCanciones),R.drawable.canciones));

        sp =(Spinner) findViewById(R.id.spinner1);
        SpinnerAdapter adapter = new SpinnerAdapter(this,R.layout.spinner_layout,R.id.lblCategorias,list);
        sp.setAdapter(adapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Toast.makeText(adapterView.getContext(),getString(R.string.msgSeleccionado).toString() + " " +((ItemData) adapterView.getItemAtPosition(i)).getTextCategoria(), Toast.LENGTH_SHORT).show();
          }
          @Override
          public void onNothingSelected(AdapterView<?> adapterView) {
          }
        });

        listView = (ListView) findViewById(R.id.spinner);
        ListAdapter lista = new SpinnerAdapter(this, R.layout.spinner_layout, R.id.lblCategorias, list);
        listView.setAdapter(lista);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
          @Override
          public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Toast.makeText(adapterView.getContext(), getString(R.string.msgSeleccionado).toString() + " " + ((ItemData) adapterView.getItemAtPosition(position)).getTextCategoria(), Toast.LENGTH_SHORT).show();
          }
        });
    }
}
